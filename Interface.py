from flask import  Flask, render_template, request, jsonify
from threading import Thread
from time import sleep
import globals
import os

apache_path = "/var/www/html/shaman/"
#info_name = apache_path + "launch_info.in"
info_name = "launch_info.in"


app = Flask(__name__)

def get_operation_type():
    operation_type = raw_input("operation (search for) type: ")
    #
    return operation_type


def get_reaction_type():
    reaction_type = raw_input("reaction type: ")
    return reaction_type

def get_launch_info():
    launch_info = open(info_name, 'w')
    launch_info.close()
    while os.stat(info_name).st_size == 0:
        sleep(5)
    launch_info = open(info_name, 'r')
    parse = launch_info.readline().strip()
    link = launch_info.readline().strip()
    launch_info.close()
    return parse, link

def get_link():
    post_link = raw_input()
    return post_link

@app.route("/", methods=["POST", "GET"])
def form():
    if request.method == "POST":
        globals.parse_type = request.form['parse']
        globals.link = request.form['link']
        return render_template("form.html")
    return render_template("form.html", parsed=globals.stat_parsed, videos=globals.stat_videos, comments=globals.stat_comments, groups='\n'.join(list(map(str, globals.groups))))


def start_app():
    app.run()

Thread(target=start_app).start()
