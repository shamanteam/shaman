from Utilits import safe_call
import vk

def parse_likes(api,post_type, owner, item, offset):
     new_users = api.likes.getList(type=post_type,owner_id=owner,item_id=item,extended=0,count=100,offset=offset,v=5.52)
     return new_users["items"], new_users["count"]

def parse_reposts(api, post_type, owner, item, offset):
    new_users = api.likes.getList(type=post_type,owner_id=owner,item_id=item,extended=0,count=100,offset=offset,filter="copies",v=5.52)
    return new_users["items"], new_users["count"]

def parse_subscribers(api, post_type, owner, item, offset):
    new_users = api.groups.getMembers(group_id=owner,sort="time_asc",count=100,offset=offset,v=5.52)
    return new_users["items"], new_users["count"]

def parse_comments(api, post_type, owner, item, offset):
    if  post_type == "post":
        new_users = api.wall.getComments(owner_id=owner, post_id=item, extended=1, count=60, offset=offset, v=5.52)
    elif post_type == "photo":
        new_users = api.photos.getComments(owner_id=owner, photo_id=item, extended=1, count=60, offset=offset, v=5.52)
    elif post_type == "video":
        new_users = api.video.getComments(owner_id=owner, video_id=item, extended=1, count=60, offset=offset, v=5.52)
    elif post_type == "topic":
        new_users = api.board.getComments(owner_id=owner*(-1), topic_id=item, extended=1, count=60, offset=offset, v=5.52)
    return [new_users["profiles"][i]["id"] for i in range(len(new_users["profiles"]))], new_users["count"]





