# coding=utf-8

import vk
import traceback
import Parsers
import random
import codecs
import globals
from text import f
from collections import deque
from threading import Thread
import Handlers
import sys
import Interface
import Auth
import time
from Utilits import *
import heapq

b = {0: 'get info', 1: 'post video', 2: 'post comment'}

globals.init()
group_id = "125968557"
bot_id = 0
handler2 = 0
handler3 = 0
api_ids = []
log_path = "log"
update_time = 1
base_offset = 60
offset = 10 ** 10
append_time = [0.4, 0.3, 84]
randrange = [1, 1, 30]

logins = []
tokens = []
ids = []
account_info = open('id_log', 'r')
line = account_info.readline().strip()
while line != '':
    id, login = line.split()
    ids.append(id)
    logins.append(login)
    line = account_info.readline().strip()
account_info.close()
flag = True
app_ids = []
# app_ids = ["5534618", "5552096", "5552108", "5552106", "5564513", "5564512", "5564510", "5568778",
#           "5568779", "5468779", "5418779", "5418219"]
password = "Shaman1"
current_time = time.time()
not_new = []
new_users = []
count = 0
times = [1]
full_access = "notify,friends,photos,audio,video,docs,notes,pages,status,offers,questions,wall,groups,messages,notifications,stats,ads,offline"


def check_queue(api):
    # log = open('bot_logs/' + str(api), 'a')
    time_start = time.time()
    sleep_after_captcha = 1800
    timer = [[time_start, 0], [time_start, 1], [time_start, 2]]
    while True:
        timer.sort()
        if timer[0][0] >= time.time():
            time.sleep(0.3)
        else:
            for i in range(3):
                if timer[i][0] <= time.time() and len(globals.d[timer[i][1]]) != 0:
                    timer[i][0] = time.time() + append_time[timer[i][1]] + random.randrange(randrange[timer[i][1]])
                    func = globals.d[timer[i][1]].popleft()
                    try:
                        # log.write(str(time.clock()) + ' trying to ' +  b[timer[i][1]])
                        func(api)
                        # log.write(': success' + '\n')
                        break
                    except:
                        # log.write(": failure" + '\n')
                        globals.d[timer[i][1]].append(func)
                        error = traceback.format_exc()
                        print error
                        if "VkAPIError: 14" in error:
                            timer[i][0] = timer[i][0] + sleep_after_captcha
                            sleep_after_captcha *= 1
                        if "VkAPIError: 9" in error:
                            if not globals.is_migrating:
                                globals.is_migrating = True
                                migrate(api_ids, random.choice(tokens))
                            else:
                                time.sleep(30)

def main():
    global bot_id, not_new, offset, handler2, handler3, app_ids
    app_base = open("app_id", "r")
    app_ids = list(map(int, app_base.readlines()))
    app_base.close()
    make_log_dir(log_path)
    for number in logins:
        token = ''
        while token == '':
            try:
                token = f(number, random.choice(app_ids))
            except:
                print traceback.format_exc()
                token = ''
        tokens.append(token)
        session = vk.Session(access_token=token)
        api = vk.API(session)
        api_ids.append(api)
        #api.groups.join(group_id=group_id)
        #time.sleep(0.4)
    globals.groups.append(-make_new_group(api_ids, random.choice(tokens)))
    #print group_id
    #globals.groups.append(group_id)
    #group_id = -126626298
    parse_type, link = 0, 0
    while parse_type == 0:
        parse_type, link = globals.parse_type, globals.link
    reaction_type = "comment"
    # parse options: likes, reposts, comments, subscribers
    # get link on post/club
    post_type, owner_id, item_id = parse_vk_link(link)
    while post_type == "incorrect input":
        link = Interface.get_link()
        post_type, owner_id, item_id = parse_vk_link(link)
    text = ", аДаОаБбаО аПаОаЖаАаЛаОаВаАбб аВ ааа а! ааДаИбаЕ аПаОаДбаОаБаНаОббаИ аВ аБаЛаИаЖаАаЙбаЕаЕ аВбаЕаМб."
    parser = lambda offset, parse_type=parse_type, api=api, post_type=post_type, owner_id=owner_id, item_id=item_id, \
        : safe_call(getattr(Parsers, "parse_" + parse_type)(api, post_type, owner_id, item_id, offset))

    # handler1 = lambda user, reaction_type="comment", api=api, post_type=post_type, owner_id=owner_id, item_id=item_id,\
    #    offset=offset, text=text : safe_call(getattr(Handlers, "reaction_" + reaction_type)(api,post_type,user,owner_id,item_id,text))

    globals.video_handler_template = lambda user, api, owner_id, reaction_type="post_video", post_type=post_type, item_id=item_id, text=" У нас есть для тебя видео"\
        : safe_call(getattr(Handlers, "reaction_" + reaction_type)(api,user,owner_id,item_id,text,post_type))

    globals.comment_handler = lambda user, api, owner_id, item_id, reaction_type="comment", post_type="video",text=" Мы ждем тебя в кино"\
        : safe_call(getattr(Handlers, "reaction_" + reaction_type)(api,post_type,user,owner_id,item_id,text))

    globals.video_handler = lambda user, api, owner_id=globals.groups[-1] : globals.video_handler_template(user, api, owner_id)

    #handlers = [handler2]#[handler1, handler2, handler3]

    log_name = log_path + "/" + parse_type + "_" + str(owner_id) + "_" + str(item_id) + "_" + reaction_type + ".log"

    file = safe_open(log_name)
    line = file.readline().strip()
    while line != '':
        not_new.append(int(line))
        line = file.readline().strip()
    file.close()
    for api in api_ids[:-1]:
        Thread(target=check_queue, args=(api,)).start()
    print api
    ping(assign, parser, log_name, api)


def assign(parser, log_name, api):
    file = open(log_name, 'a')
    global current_time, not_new, offset, Q, new_users, count
    if time.time() - current_time >= update_time:
        try:
            current_time = time.time()
            for user in new_users[::-1]:
                if not user in not_new:
                    globals.get_info_queue.append(lambda api, user=user: get_info(id=user, api=api))
                    # for i in range(len(handlers)):
                    # heapq.heappush(Q, [time.time() + times[i], lambda api, user=user : handlers[i](user=user, api=api)])
                    file.write(str(user) + '\n')
                    not_new.append(user)
            prev_count = count
            # sleep(0.6)
            new_users, count = parser(offset)
            offset = min(max(0, offset - 10 + (count - prev_count)), count)
            new_users, count = parser(offset)
            '''print "count:", count
            print "offset:", offset
            print "len(not_new):", len(not_new)
            print "\n" + "\n"'''
        except Exception as ex:
            # print user, "bad"
            errors_log = open("errors_log.log", "a")
            print traceback.format_exc()
            if traceback.format_exc().find("VkAPIError: 15. Access denied: user deactivated.") != -1:
                deleted_user = traceback.format_exc()[
                               traceback.format_exc().find("'user_id': u'") + len("'user_id': u'"): \
                                   traceback.format_exc().find("', u'offset': u'")]
                print deleted_user
            not_new.append(user)
            file.write(str(user) + '\n')
            errors_log.write(str(time.time()) + "\n" + traceback.format_exc() + '\n')
            errors_log.close()
            print traceback.format_exc().split('\n')[-2]
    file.close()


if __name__ == "__main__":
    main()