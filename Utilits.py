# coding=utf-8

import os
import Handlers
import time
from time import sleep
import requests
import vk
import globals


def safe_call(func):
    time.sleep(0.2)
    return func

def write_to_sql(class_object):
    db_session = globals.Session()
    try:
        db_session.add(class_object)
        db_session.commit()
    except:
        db_session.rollback()
    finally:
        globals.Session.remove()

def vkMethod(method, **kwargs):
    vkToken = "f68a67200777f043f6717b6725d182dbe83eb2f85ac10c40bc5fefc43cd7fe5ff50838faca2f8fbc20ded"
    url = 'https://api.vk.com/method/%s.json' % (method)
    kwargs.update({'access_token': vkToken})
    response = requests.post(url, kwargs).json()
    return response

def safe_open(file_path, param="r"):
    try: opened_file = open(file_path, param)
    except:
        opened_file = open(file_path, 'w')
        opened_file.close()
        opened_file = open(file_path, param)
    return opened_file


def parse_vk_link(link):
    if link.find("wall") != -1:
        link = link[link.find("wall") + len("wall"):]
        return ['post'] + list(map(int, link.split("_")))
    elif link.find("photo") != -1:
        link = link[link.find("photo") + len("photo"):]
        if link.find('%') != -1:
            link = link[:link.find('%')]
        return ['photo'] + list(map(int, link.split("_")))
    elif link.find("video") != -1:
        link = link[link.find("video") + len("video"):]
        if link.find('%') != -1:
            link = link[:link.find('%')]
        return ['video'] + list(map(int, link.split("_")))
    elif link.find("topic") != -1:
        link = link[link.find("topic") + len("topic"):]
        if link.find('%') != -1:
            link = link[:link.find('%')]
        return ['topic'] + list(map(int, link.split("_")))
    elif link.find("club") != -1:
        link = link[link.find("club") + len("club"):]
        if link.find('%') != -1:
            link = link[:link.find('%')]
        return ['club'] + list(map(int, link.split("_")))
    return ['incorrect input', 0, link[link.rfind("/") + len("/"):]]


def make_log_dir(log_path):
    exist = os.path.exists(log_path)
    if not exist:
        os.mkdir(log_path)
    return


def ping(func, *args):
    while True:
        func(*args)

def get_info(id,api):
    atribute = "first_name,last_name,city,sex,photo_max,connections,contacts,universities,schools,bdate,home_town"
    #print id
    info = api.users.get(user_ids=id, fields=atribute, v=5.53)[0]
    #friends_info = vkMethod(method="friends.get", user_id=id, offset=0, count=4, order="random", name_case="nom",v=5.53)["response"]
    friends_info = api.friends.get(user_id=id, offset=0, count=16, order="random", fields="photo_max", name_case="nom", v=5.53)["items"]
    sleep(0.4)
    photos_info = api.photos.getAll(owner_id=id,count=6,extended=0,offset=0,skip_hidde=1,v=5.53)
    sleep(0.4)
    photos_info = [photos_info["items"][i]["photo_604"] for i in range(len(photos_info["items"]))]
    user = globals.User(info=info, friends_info=friends_info, photos_info=photos_info)
    globals.post_video_queue.append(lambda api, user=user.id : globals.video_handler(user=user, api=api))
    write_to_sql(user)
    globals.stat_parsed += 1
    #write_to_pickle(class_object=user, file_name="pickle/" + str(user.id) + ".db")
    '''
    photo_str = ""
    file = open("log.db","a")
    #log_text = user.first_name.encode("utf-8")
    #print user.sex, user.first_name
    log_text = str(user.id) + "\n" + \
        user.first_name.encode("utf-8") + " " + user.last_name.encode("utf-8") + '\n' + \
        "sex: " + user.sex + "\n" + "city: " + user.city.encode("utf-8") + "\n" + \
        "photo: " + user.photo.encode("utf-8") + "\n" + \
        " ".join(list(map(lambda x: x.encode("utf-8"), user.photos))) \
        #+ "\n".join(list(map()))
    #print log_text
    #file.write(log_text + '\n' + '\n' + '\n')
    #file.close()
    '''
    return

def make_new_group(bots, token):
    #print "Group Creating..."
    #app_id = "5552096"
    #email = "valentin.biryukov@gmail.com"
    #password = "incognito9"
    #full_access = "notify,friends,photos,audio,video,docs,notes,pages,status,offers,questions,wall,groups,messages,notifications,stats,ads,offline"
    #token, user_id = Auth.auth(email, password, app_id, full_access)
    #token = '3beba43c5da609ea8812844d0c3ca0b3345b44912646056dc4206998508d7a9407454a60aa53ed7cfd380'
    session = vk.Session(access_token=token)
    api = vk.API(session)
    group = api.groups.create(title="Hobby",v=5.53)
    sleep(0.3)
    print group
    api.groups.edit(group_id=group["id"],wall=0,topics=0,photos=2,video=2,audio=0,docs=0,wiki=0,messages=0,v=5.53)
    for bot in bots:
        try:
            bot.groups.join(group_id=group["id"],v=5.53)
            sleep(0.4)
        except:
            pass
    members = api.groups.getMembers(group_id=group["id"],v=5.53)["items"]
    sleep(0.3)
    for bot in members:
        try:
          api.groups.editManager(group_id=group["id"],user_id=bot,role="editor",v=5.53)
          sleep(0.3)
        except:
            print bot
            sleep(0.3)
    print group
    return group["id"]

def migrate(bots, token):
    globals.groups.append(-make_new_group(bots, token))
    globals.video_handler = lambda user, api, owner_id=globals.groups[-1] : globals.video_handler_template(user, api, owner_id)
    globals.is_migrating = False



