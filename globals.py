# coding=utf-8
from collections import deque
from sqlalchemy.orm import sessionmaker, mapper, scoped_session
import sqlalchemy as sql
import re

class User(object):
    def __init__(self, info, friends_info, photos_info):
        self.id = info["id"]
        self.first_name = info["first_name"]
        self.last_name = info["last_name"]
        try:
            self.main_photo = info["photo_max"]
        except Exception:
            self.main_photo = u"https://vk.com/images/camera_200.png"
        self.photos = u" ".join(photos_info)
        self.friends = u", ".join([elem["first_name"] + u" " + elem["last_name"]  for elem in friends_info])
        if info["sex"] == 1:
            self.sex = u"female"
        elif info["sex"] == 2:
            self.sex = u"male"
        else:
            self.sex = u"none"
        try:
            self.city = info["city"]["title"]
        except:
            self.city = u"none"

        try:
            self.university = info["universities"][0]["name"]
        except:
            self.university = u"none"

        try:
            self.school = info["schools"][0]["name"]
        except:
            self.school = u"none"

        try:
            if re.match(r'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$', info['mobile_phone']):
                self.phone = info["mobile_phone"]
            else:
                self.phone = u"none"
        except:
            self.phone = u"none"

        try:
            self.skype = info["skype"]
        except:
            self.skype = u"none"

        try:
            self.birthdate = info["bdate"]
        except:
            self.birthdate = u"none"

        try:
            self.hometown = info["home_town"]
            if len(self.hometown) == 0:
                self.hometown = u"none"
        except:
            self.hometown = u"none"

    def __str__(self):
        return self.first_name + " " + self.last_name

#get_info_queue, post_video_queue, post_comment_queue, handler2, handler3, d, Session

def init():
    global get_info_queue, post_video_queue, post_comment_queue, video_handler, comment_handler, video_handler_template, d,Session, stat_parsed, stat_videos, stat_comments, groups, is_migrating, parse_type, link
    parse_type = 0
    link = 0
    stat_comments = 0
    stat_parsed = 0
    stat_videos = 0
    groups = []
    is_migrating = False
    get_info_queue = deque()
    post_video_queue = deque()
    post_comment_queue = deque()
    d = {0: get_info_queue, 1: post_video_queue, 2: post_comment_queue}
    engine = sql.create_engine("sqlite:///sqlalchemy.db", echo=False, connect_args={'check_same_thread':False})
    metadata = sql.MetaData()
    users_table = sql.Table('users', metadata,
                        sql.Column("first_name", sql.String),
                        sql.Column("last_name", sql.String),
                        sql.Column("city", sql.String),
                        sql.Column("sex", sql.String),
                        sql.Column("main_photo", sql.String),
                        sql.Column("photos", sql.String),
                        sql.Column("friends", sql.String),
                        sql.Column("skype", sql.String),
                        sql.Column("phone",sql.String),
                        sql.Column("school", sql.String),
                        sql.Column("birthdate", sql.String),
                        sql.Column("hometown", sql.String),
                        sql.Column("university", sql.String),
                        sql.Column("id", sql.Integer, primary_key=True))

    metadata.create_all(engine)
    Session = scoped_session(sessionmaker(bind=engine))
    mapper(User, users_table)