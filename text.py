import Auth
import traceback

app_id = "5534618"
full_access = "notify,friends,photos,audio,video,docs,notes,pages,status,offers,questions,wall,groups,messages,notifications,stats,ads,offline"

import signal

from functools import wraps
import errno
import os
import signal

class TimeoutError(Exception):
    pass

def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator

@timeout(5)
def f(number, app_id=app_id):
    token, user_id = Auth.auth(number,  "Shaman1", app_id, full_access)
    return token

fin = open('id_log', 'r')
ids = []
numbers = []
line = fin.readline().strip()
while line != '':
    id, number = line.split()
    ids.append(id)
    numbers.append(number)
    line = fin.readline().strip()
tokens = []
for i in range(len(ids)):
    while len(tokens) < (i + 1):
        try:
            tokens.append(f(numbers[i]))
            print("success:", ids[i])
        except:
            print("fail:", ids[i])
        #print traceback.format_exc()
fin.close()
print ids
print numbers
print tokens